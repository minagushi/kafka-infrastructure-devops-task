resource "tls_private_key" "keygen" {
  algorithm = "RSA"
}

resource "aws_key_pair" "this" {
  key_name   = module.eks_naming.id
  public_key = tls_private_key.keygen.public_key_openssh

  lifecycle {
    create_before_destroy = true
  }
  tags = var.tags
}

resource "aws_ssm_parameter" "workers_ssh_key" {
  name        = format("/%s/%s/ec2_kafka/ssh_key", var.project, var.env)
  description = "Kafka Servers SSH key"
  type        = "SecureString"
  value       = tls_private_key.keygen.private_key_pem

  tags = var.tags
}

module "kafka_broker" {
  count   = var.kafka_broker.instance_count
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = format("kafka-broker-%", count.index)

  ami                    = var.kafka_broker.ami
  instance_type          = var.kafka_broker.instance_type
  key_name               = aws_key_pair.this.name
  monitoring             = var.kafka_broker.monitoring
  vpc_security_group_ids = [data.terraform_remote_state.vpc.outputs.vpc.kafka_broker_sg_id]
  subnet_id              = data.terraform_remote_state.vpc.outputs.vpc.private_subnet

  tags = local.tags
}

module "kafka_rest" {
  count   = var.kafka_broker.instance_count
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = format("kafka-broker-%", count.index)

  ami                    = var.kafka_broker.ami
  instance_type          = var.kafka_broker.instance_type
  key_name               = aws_key_pair.this.name
  monitoring             = var.kafka_broker.monitoring
  vpc_security_group_ids = [data.terraform_remote_state.vpc.outputs.vpc.kafka_broker_sg_id]
  subnet_id              = data.terraform_remote_state.vpc.outputs.vpc.private_subnet

  tags = merge(
    local.tags,
    {
      app = "broker"
    }
  )
}

module "kafka_rest" {
  count   = var.kafka_rest.instance_count
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = format("kafka-broker-%", count.index)

  ami                    = var.kafka_rest.ami
  instance_type          = var.kafka_rest.instance_type
  key_name               = aws_key_pair.this.name
  monitoring             = var.kafka_rest.monitoring
  vpc_security_group_ids = [data.terraform_remote_state.vpc.outputs.vpc.kafka_broker_sg_id]
  subnet_id              = data.terraform_remote_state.vpc.outputs.vpc.private_subnet

  tags = merge(
    local.tags,
    {
      app = "zookeeper"
    }
  )
}

module "zookeeper" {
  count   = var.zookeeper.instance_count
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = format("kafka-broker-%", count.index)

  ami                    = var.zookeeper.ami
  instance_type          = var.zookeeper.instance_type
  key_name               = aws_key_pair.this.name
  monitoring             = var.zookeeper.monitoring
  vpc_security_group_ids = [data.terraform_remote_state.vpc.outputs.vpc.kafka_broker_sg_id]
  subnet_id              = data.terraform_remote_state.vpc.outputs.vpc.private_subnet

  tags = merge(
    local.tags,
    {
      app = "zookeeper"
    }
  )
}