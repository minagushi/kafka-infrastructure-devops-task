provider "aws" {
  region = "us-west-2"
}

data "aws_caller_identity" "current" {}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "project" {
  type    = string
  default = "test-project"
}

variable "service" {
  type    = string
  default = "kafka"
}

variable "env" {
  type    = string
  default = "dev"
}

locals {
  tags = {
    project     = var.project
    environment = var.env
    service     = var.service
    managed-by  = "terraform"
  }
}