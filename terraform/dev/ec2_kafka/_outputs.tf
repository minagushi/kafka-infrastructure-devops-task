output "kafka_broker" {
  value = module.kafka_broker
}

output "kafka_rest" {
  value = module.kafka_rest
}

output "zookeeper" {
  value = module.zookeeper
}