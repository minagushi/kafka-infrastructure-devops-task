terraform {
  backend "s3" {
    bucket         = "kafka-infrasturcture-terraform-state"
    key            = "uat/ec2_kafka"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock"
  }
}