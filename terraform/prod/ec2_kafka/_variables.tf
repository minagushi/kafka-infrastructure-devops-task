variable "kafka_broker" {
  type        = map(sting)
  description = "Parameters for Kafka broker instances"
}

variable "kafka_rest" {
  type        = map(sting)
  description = "Parameters for zookeeper instances"
}

variable "zookeeper" {
  type        = map(sting)
  description = "Parameters for zookeeper instances"
}