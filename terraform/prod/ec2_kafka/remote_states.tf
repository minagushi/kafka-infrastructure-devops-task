data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "kafka-infrasturcture-terraform-state"
    prefix = "shared/vpc"
  }
}