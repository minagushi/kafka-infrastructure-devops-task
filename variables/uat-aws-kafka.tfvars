kafka_broker = {
  instance_count = 2
  instance_type = "m4.large"
  monitoring = true
}

kafka_rest = {
  instance_count = 2
  instance_type = "m4.large"
  monitoring = true
}

zookeeper = {
  instance_count = 2
  instance_type = "m4.large"
  monitoring = true
}
