kafka_broker = {
  instance_count = 1
  instance_type = "t3.medium"
  monitoring = false
}

kafka_rest = {
  instance_count = 1
  instance_type = "t3.medium"
  monitoring = false
}

zookeeper = {
  instance_count = 1
  instance_type = "t3.medium"
  monitoring = false
}
