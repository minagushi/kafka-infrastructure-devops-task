kafka_broker = {
  instance_count = 3
  instance_type = "m4.xlarge"
  monitoring = true
}

kafka_rest = {
  instance_count = 3
  instance_type = "m4.xlarge"
  monitoring = true
}

zookeeper = {
  instance_count = 3
  instance_type = "m4.xlarge"
  monitoring = true
}
