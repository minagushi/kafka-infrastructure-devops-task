# Kafka Infrastructure Devops Task

## Initiall Task Text

* Design a technical solution for the gitops pipeline. The goal is to provide provisioning/configuration for distributed application management
* Try to describe a branching strategy for git (considering this is monorepo with directories/modules for multiple projects, each has specific infrastructure requirements)
* Once the change is detected in the Ansible playbooks repository, the pipeline should be executed on the following environments: DEV-> UAT-> PROD (in this order, in case of failure cannot proceed to the next, optionally this should be avoided with listener/handler)
* E.g. basic provisioning + configuration management for Kafka and promotion to all environments
(use partial implementation, pseudocode, diagrams or describe, it is not necessary to deliver a completesolution. Target is to prove your point)

Tools: GIT, Ansible (optionally shell/python)

## Design
* Infrastructure based on AWS for simplicity
* for CI\CD I'm using gitlab as it recently used by my projects.
* I use include for pipeline as it's more convinienr way to manage and read the code.
* For terraform public modules used for same reason as AWS, simplicity (I wouldn't use it in working environment as each company and product has different specifics so I prefer to write my own)
* For each environment(dev|uat|stage) there's it's own set of variables stored in variables/ folder, it includes ansible_vars.yaml for Kafka configuration and terrafomr.tfvars for infrastructure provisioned by terraform.
* Ansible utilise dynamic AWS inventory for convinience, nodes separated by tags
### Basic flow will be:
1. Developer push dev commit it will trigger pipline running develop branch steps
2. terraform provision infrastructure, ansible play configuration with dev variables from variables/ folder
3. run tests agains service
4. Destory infrastructure
5. Developer open merge request
6. After code reviewe code goes to master
7. Master triggers pipline with UAT steps
8. Same proceess as for development apply, terraform updating/provision UAT infrasturcture, ansible play configuration with UAT variables.
9. Run tests agains service
10. On UAT pipeline success pipline will create new version tag from master
11. New version tag eventually trigger new pipline with production steps
12. Same as prvious, on falure pipline will rerun previous successfull run to rollback configuration

![alt text](https://gitlab.com/minagushi/kafka-infrastructure-devops-task/-/raw/79c23ce33a50100c378bd1fd1ebde419802a481f/pipeline.png)


### Conciderations and thoughts
* Didn't work with kafka so I just get some pices of available ansible solution just to have some fullfilment for repo. https://github.com/confluentinc/cp-ansible/
* In actual environment I'd rather write my own modules than reuse public.
* Any sensible information either store in secrets manager or CI protected variables.
* For CI/CD i'd rather build my own dockerimages and store im private image registry, so as modules and roles good to keep in artifactory and have separated repo.
* considering this is monorepo with directories/modules for multiple projects I'd add some additional steps triggered by path using gilab ci rules such as 
```
changes:
  - folder/some_folder/**/* 
```
